#
#
#
PREFIX	= /usr
DESTDIR	=
PACKAGE = alice-template
VERSION = 0.6
distdir = $(PACKAGE)-$(VERSION)
bindistdir = $(PACKAGE)-$(VERSION)-bin

# Define this for system installation 
# SYSTEM	= 1
ifdef VERBOSE
M		:=
REDIR		:=
else
M		:= @
REDIR		:= > /dev/null 2>&1
endif

all:	test

clean:
	@echo "CLEAN"
	$(MAKE) distclean -C latex
	$(MAKE) clean -C odf 
	$(MAKE) clean -C root 


install:
	@echo "INSTALL $(DESTDIR)"
	$(MAKE) install -C latex
	$(MAKE) install -C odf
	$(MAKE) install -C root

uninstall:
	@echo "UNINSTALL $(DESTDIR)"
	$(MAKE) uninstall -C latex
	$(MAKE) uninstall -C odf
	$(MAKE) uninstall -C root

test:
	@echo "TEST"
	$(MAKE) test -C root
	$(MAKE) test -C latex
	$(MAKE) test -C odf

distdir:
	@echo "DISTDIR $(DESTDIR)$(distdir)"
	$(M)mkdir -p $(DESTDIR)$(distdir) 
	$(M)cp Makefile README.md $(DESTDIR)$(distdir) 
	$(M)$(MAKE) dist -C latex distdir=../$(distdir)
	$(M)$(MAKE) dist -C odf   distdir=../$(distdir)
	$(M)$(MAKE) dist -C root  distdir=../$(distdir)

dist:	distdir
	@echo "DIST $(distdir)"
	$(M)tar -czvf $(distdir).tar.gz $(distdir) 
	$(M)rm -rf $(distdir)

distcheck: dist
	@echo "DIST(CHECK) $(distdir)"
	$(M)tar -xzvf $(distdir).tar.gz 
	$(MAKE) -C $(distdir) test
	$(M)rm -rf $(distdir)

bindistdir:
	@echo "DISTDIR(BIN) $(bindistdir)"
	$(M)mkdir -p $(DESTDIR)$(bindistdir) 
	$(M)cp README.md $(DESTDIR)$(bindistdir) 
	$(MAKE) bindist -C latex bindistdir=../$(bindistdir)
	$(MAKE) bindist -C odf   bindistdir=../$(bindistdir)
	$(MAKE) bindist -C root  bindistdir=../$(bindistdir)

bindist:bindistdir
	@echo "DIST(BIN) $(bindistdir)"
	$(M)tar -czvf $(bindistdir).tar.gz $(bindistdir) 
	$(M)rm -rf $(bindistdir)


tag:
	git tag $(VERSION) 

docker:
	docker run --user root --group-add users -e GRANT_SUDO=yes -it --rm \
		-v $(PWD):/root/$(notdir $(PWD)) texlive/texlive \
		/bin/bash

#
#
#


