ALICE Templates and Scripts
===========================

This package contains:

- Templates: 
  - ODF (LibreOffice/OpenOffice.org) templates for presentations and
    posters.
  - LaTeX beamer styles for presentations and posters 
- ROOT scripts:
  - For drawing the ALICE logo using vector graphics
  - For consistent look of figures 

The style is mimicked as close as possible to the _[official][]_ templates
provided to the collaboration.

Downloads:
----------

The project is managed at [GitLab][].  Go there to get the newest
sources and documentation.

### The most recent

- [Documentation][doc-master]
- [Distribution][dist-master]
- [Pre-built distribution][bindist-master]
	
### Version 0.6

- [Documentation][doc-0_6]
- [Distribution][dist-0_6]
- [Pre-built distribution][bindist-0_6]

Installation (from sources):
----------------------------

For users: 

    make install

For system administrators:

    make install SYSTEM=1

Installation (from pre-built):
------------------------------

For LibreOffice/OpenOffice templates:

    cd odf 
    unopkg add alice_templates.oxt

For LaTeX classes and styles:

    mkdir -p ~/texmf/tex/latex/beameralice
    cd latex 
    cp *.sty *.cls *.png *.pdf *.eps ~/texmf/tex/latex/beameralice/
    
    (you may also want to copy the manual alice_template.pdf and the
    examples to somewhere e.g., ~/texmf/doc/latex/beameralice)

For ROOT scripts:

    mkdir -p ~/root/macros 
    cd root
    cp *.C ~/root/macros/

    (and add ~/root/macros to your macro search path in ~/.rootrc)

Christian Holm Christensen <cholm@nbi.dk>


[official]: http://aliweb.cern.ch/charter "official"
[GitLab]: https://gitlab.cern.ch/cholm/alice-templates
[doc-master]: https://gitlab.cern.ch/cholm/alice-templates/builds/artifacts/master/file/latex/alice_template.pdf?job=doc
[dist-master]: https://gitlab.cern.ch/cholm/alice-templates/builds/artifacts/master/file/alice-template-master.tar.gz?job=dist
[bindist-master]: https://gitlab.cern.ch/cholm/alice-templates/builds/artifacts/master/file/alice-template-master-bin.tar.gz?job=bindist
[doc-0_6]: https://gitlab.cern.ch/cholm/alice-templates/builds/artifacts/0.6/file/latex/alice_template.pdf?job=doc
[dist-0_6]: https://gitlab.cern.ch/cholm/alice-templates/builds/artifacts/0.6/file/alice-template-0.6.tar.gz?job=dist
[bindist-0_6]: https://gitlab.cern.ch/cholm/alice-templates/builds/artifacts/0.6/file/alice-template-0.6-bin.tar.gz?job=bindist
