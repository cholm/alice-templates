ROOT scripts for the ALICE collaborator
=======================================

This are in some ways obsolete but kept for reference 

- `AliceLogo.C` Draw ALICE logo in TPad using vector graphics
- `AlicePlot.C` Utilities for ALICE plots. 
- `LogoTest.C` Test of `AliceLogo.C`
- `PlotTest.C` Test of `AlicePlot.C`

Christian
