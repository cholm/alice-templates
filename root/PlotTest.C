TH1* MakeSpectrum(const char* name, const char* title, 
		  Int_t pdg, Double_t a, Double_t b, TF1* f,
		  Double_t scale=1)
{
  if (f) f->SetParameters(a, b);
  TH1* h1 = new TH1F(name, title, f ? 20 : 25, f ? .5 : 0, f ? 3.5 : 5);
  h1->Sumw2();
  h1->SetMarkerColor(AlicePlot::GetParticleColor(pdg));
  h1->SetMarkerStyle(AlicePlot::GetParticleMarker(pdg));
  if (f) {
    h1->FillRandom(f->GetName(), a * 100000);
    h1->Scale(scale / h1->GetEntries());
  }
  h1->SetXTitle(Form("%s [GeV]", AlicePlot::Pt()));
  h1->SetYTitle(AlicePlot::TextdNdpt());
  // h1->Scale(1. / h1->GetEntries());
  return h1;
}

void PlotSpectra()
{
  // --- A function --------------------------------------------------
  TF1* f = new TF1("spectrum", "expo", .5, 4);
  f->SetParameters(1, -1);

  // --- Frame -----------------------------------------------
  TH1* base = MakeSpectrum("base", "base", 0, 0, 0, 0, 100);
  base->SetMaximum(10);
  base->SetMinimum(0.0001);
  base->Draw("axis");
  

  // --- pi+ histogram -----------------------------------------------
  TH1* pip = MakeSpectrum("pip", "#pi^{+}#times10", 211, 1, -2, f, 10);
  pip->Draw("same E1 X0");

  // --- pi- histogram -----------------------------------------------
  TH1* pim = MakeSpectrum("pim", "#pi^{-}", -211, 1, -2.1, f, 10);
  pim->Draw("same E1 X0");

  // --- k+ histogram ------------------------------------------------
  TH1* kp = MakeSpectrum("kp", "K^{+}#times3", 321, 1, -2, f, 3);
  kp->Draw("same E1 X0");

  // --- k- histogram ------------------------------------------------
  TH1* km = MakeSpectrum("km", "K^{-}", -321, 1, -2.1, f, 3);
  km->Draw("same E1 X0");

  // --- p histogram -------------------------------------------------
  TH1* pp = MakeSpectrum("pp", "p", 2212, 1, -2, f, 1);
  pp->Draw("same E1 X0");

  // --- pbar histogram ----------------------------------------------
  TH1* pm = MakeSpectrum("pm", "#bar{p}", -2212, 1, -2.1, f, 1);
  pm->Draw("same E1 X0");
  
  // --- First graph -------------------------------------------------
  // Note the use of AlicePlot::GetMarkerStyle 
  TGraph* g1 = new TGraph(2);
  AlicePlot::SetGraphStyle(g1, -1, -1, kBlue+1);
  g1->SetMarkerStyle(AlicePlot::GetMarkerStyle(AlicePlot::kStar|
					       AlicePlot::kHollow));
  g1->SetPoint(0, .5,  pim->GetBinContent(1));
  g1->SetPoint(1, 3.5, pim->GetBinContent(20));
  g1->Draw("lp same");

  // --- Second graph ------------------------------------------------
  // Marker style is flipped relative to g1 above 
  TGraph* g2 = new TGraph(2);
  AlicePlot::SetGraphStyle(g2, -1, -1, kBlue+1);
  g2->SetMarkerStyle(AlicePlot::FlipHollowStyle(g1->GetMarkerStyle()));;
  g2->SetPoint(0, .5,  pip->GetBinContent(1));
  g2->SetPoint(1, 3.5, pip->GetBinContent(20));
  g2->Draw("lp same");
  
  // --- Draw a legend -----------------------------------------------
  // Note, we split it in two columns and add dummy entries as headers
  TLegend* l = new TLegend(.7, .75, .9, .9);
  l->SetNColumns(2);
  AlicePlot::SetLegendStyle(l, .03);
  l->AddEntry("d1", "Postive", "");
  l->AddEntry("d1", "Negative", "");
  l->AddEntry(pip, pip->GetTitle(),     "p");
  l->AddEntry(pim, pim->GetTitle(),     "p");
  l->AddEntry(kp,  kp->GetTitle(),      "p");
  l->AddEntry(km,  km->GetTitle(),      "p");
  l->AddEntry(pp,  pp->GetTitle(),      "p");
  l->AddEntry(pm,  pm->GetTitle(),      "p");
  l->AddEntry(g2, "A graph",            "lp");
  l->AddEntry(g1, "A graph",            "lp");
  l->Draw();

  // --- Draw system information -------------------------------------
  AlicePlot::DrawSystem(-1, -1, AlicePlot::kPP, 0.9);

  // --- Easy way to add Logo ----------------------------------------
  // For performance, add date
  AlicePlot::DrawLogo(-1,-1,-1,AlicePlot::kPerformance, "");
  // AlicePlot::DrawLogo(-1,-1,-1,AlicePlot::kPreliminary,"text l3 many");

  // --- Flexible way to add logo ------------------------------------
  // gROOT->LoadMacro("AliceLogo.C");
  // AliceLogo* al = new AliceLogo();
  // al->Draw(0, .75, .4, .25, "preliminary");
  // al->Draw(0, .79, .35, .25, "performance");
  // al->Draw(0, .75, .4, .25, "","text l3 many particles");
  // al->Draw(0, .79, .35, .25, "preliminary", "text l3 many particles");

  // --- Possible add a date -----------------------------------------
  // AlicePlot::DrawDate();
}

TH1* MakedNdeta(Int_t cMin, Int_t cMax, Double_t scale=1)
{
  Bool_t frame = (cMin == 0 && cMax == 0);
  TH1* h1 = new TH1F(Form("c%03d_%03d", cMin, cMax),
		     Form("%3d%% - %3d%%", cMin, cMax), 
		     frame ? 25 : 20, 
		     frame ? -4 : -3, 
		     frame ? 7  : 5);
  h1->Sumw2();
  Double_t cAvg = (cMin + cMax) / 2;
  h1->SetMarkerColor(AlicePlot::GetCentralityColor(cAvg));
  h1->SetMarkerStyle(AlicePlot::GetCentralityMarker(cAvg));
  if (!frame) {
    h1->FillRandom("gaus", 10000);
    h1->Scale(scale / h1->GetMaximum());
  }
  h1->SetXTitle("#eta");
  h1->SetYTitle(AlicePlot::TextdNdeta());
  // h1->Scale(1. / h1->GetEntries());
  return h1;
}

void PlotdNdeta()
{

  // --- Frame -----------------------------------------------
  TH1* base = MakedNdeta(0, 0, 0);
  base->SetMaximum(50000);
  base->SetMinimum(0.1);
  base->GetYaxis()->SetTitleOffset(1.2);
  base->Draw("axis");

  TLegend* l = new TLegend(.75, .65, .9, .95, "Centrality");
  // l->SetNColumns(2);
  AlicePlot::SetLegendStyle(l, .03);

  Int_t    cs[] = { 0,     5,     10,   20,   30,   40,   50,   60, 
		    80,    100, -1 };
  Double_t ss[] = { 14000, 12000, 8700, 6700, 4000, 2500, 1500, 1000, 
		    500 };
  Int_t*    pc  = cs;
  Double_t* ps  = ss;
  while (*pc < 100) { 
    Int_t cMin = *pc;
    pc++;
    Int_t cMax = *pc;
    TH1* h = MakedNdeta(cMin, cMax, *ps);
    ps++;
    l->AddEntry(h, h->GetTitle(), "pl");
    h->Draw("same E2 X0");
  }
  l->Draw();
  
  // --- Draw system information -------------------------------------
  AlicePlot::DrawSystem(-1, -1, AlicePlot::kPbPb, 2.76);

  // --- Easy way to add Logo ----------------------------------------
  // For performance, add date
  AlicePlot::DrawLogo(-1,-1,-1,AlicePlot::kPreliminary,
		      "text l3 many particles");

  // AlicePlot::DrawDate(.1, .1);
  
}

void PlotTest()
{
  // --- Plotting utilities ------------------------------------------
  gROOT->LoadMacro("AlicePlot.C");

  // --- Style -------------------------------------------------------
  AlicePlot::SetGlobalStyle();

  // --- Make the canvas ---------------------------------------------
  TCanvas* c = new TCanvas("c", "c", 600, 1000);
  AlicePlot::SetCanvasStyle(c);

  c->Divide(1, 2);

  TVirtualPad* p = c->cd(1);
  AlicePlot::SetPadStyle(p);
  p->SetLogy();
  PlotSpectra();

  TVirtualPad* p = c->cd(2);
  AlicePlot::SetPadStyle(p);
  p->SetLeftMargin(0.15);
  p->SetLogy();
  PlotdNdeta();
 
  c->Print("PlotTest.png");
  c->Print("PlotTest.pdf");
  c->Print("PlotTest.eps");
}
