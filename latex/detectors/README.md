Example of stepping through detectors in a presentation
=======================================================

The code in this directory shows how to step through all or 
some of the ALICE detectors in a presentation. 

The sequence is as follows

1. We start out with an image of ALICE in full colour
2. Then, as we go on, we 
  - replace the ALICE image with a gray-scale
    version, but with a given detector highlighted
  - show a zoom of the current detector with lines connecting
    to a box on the ALICE image to show the location of 
    the detector 
    
The user can freely configure which detectors to show and 
in what order they will be shown.  

For an example, see the LaTeX/Beamer file 

    alice-detectors.tex
    
Christian
