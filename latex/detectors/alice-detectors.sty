\ProvidesPackage{alice-detectors}
\RequirePackage{tikz}

%% Some pictures we'll use 
\tikzset{%
  %% 1 - Name
  %% 2 - width of zoom box 
  %% 3 - height of zoom box
  %% 4 - first corner to connect 
  %% 5 - second corner to connect 
  %% 6 - coordinates of zoom
  %% 7 - width of zoom
  pics/alicezoom/.style n args={7}{
    code = {%
      \node[draw=@l@d@fc,minimum width=#2cm,minimum height=#3cm](#1_box) %
      at (0,0) {}; %%
      \node[draw=@l@d@fc,thick,inner sep=0](#1_zoom) at #6 {%
        \includegraphics[width=#7cm]{\@lice@det@dir/#1_zoom}};
      \draw[color=@l@d@fc] (#1_box.#4) -- (#1_zoom.#4);
      \draw[color=@l@d@fc] (#1_box.#5) -- (#1_zoom.#5);
    }},
  %% 1 - Name
  pics/alicefull/.style = {
    code = {%
      \node at (0,0) {\includegraphics[width=10cm]{\@lice@det@dir/#1_overview}};
    }},
}  

% 
% Syntax
%
% \AliceDetectors*[detector list]*[color]{scale}
%
% With star, also show grid
% detector list is a comma separated list (ordered)
% color is colour to use for highlighting detectors 
% scale is the overall scale to apply (default dimension (10x11)cm^2)
%
\DeclareRobustCommand\AliceDetectors{%
  \let\@lice@det@grid\relax%
  \@ifstar\@lice@det@star\@lice@det}
\def\@lice@det@star{
  \def\@lice@det@grid{%
    \draw[step=0.5,gray,very thin] (-5.0,-7.5) grid (5.0,3.5);}%
  \@lice@det}
\def\AliceAllDetectors{spd,sdd,ssd,tpc,trd,tof,hmpid,emcal,phos,fmd,v0,t0,zdc,muon,acorde}
\newcommand\@lice@det[1][\AliceAllDetectors]{%
  \edef\@lice@dets{#1}
  \@lice@@det}
\def\@lice@@det{%
  \def\@lice@det@dir{small}%
  \@ifstar\@lice@@det@star\@lice@@@det}
\def\@lice@@det@star{%
  \def\@lice@det@dir{medium}%
  \@lice@@@det}
\newcommand\@lice@@@det[2][red!80!black]{%
  \colorlet{@l@d@fc}{#1}
  \begin{tikzpicture}[
    % inner sep=0pt,
    scale=#2,
    every node/.style={transform shape},
    %% SPD
    pics/alicespd/.style = {
      code = {
        \pic at (0,0) {alicefull=SPD};
        \pic at (-1.8,.01) {%
          alicezoom={SPD}{.9}{.6}{north east}{south west}{(3.4,-5.55)}{5}};
      }},
    %% SDD
    pics/alicesdd/.style = {
      code = {
        \pic at (0,0) {alicefull=SDD};
        \pic at (-1.8,.01) {%
          alicezoom={SDD}{.9}{.6}{north east}{south west}{(3.4,-5.55)}{5}};
      }},
    %% SSD
    pics/alicessd/.style = {
      code = {
        \pic at (0,0) {alicefull=SSD};
        \pic at (-1.8,.01) {%
          alicezoom={SSD}{.9}{.6}{north east}{south west}{(3.4,-5.55)}{5}};
      }},
    %% ITS
    pics/aliceits/.style = {
      code = {
        \pic at (0,0) {alicefull=ITS};
        \pic at (-1.8,.01) {%
          alicezoom={ITS}{.9}{.6}{north east}{south west}{(3.4,-5.55)}{5}};
      }},
    %% TPC
    pics/alicetpc/.style = {
      code = {
        \pic at (0,0) {alicefull=TPC};
        \pic at (-1.5,-.08) {%
          alicezoom={TPC}{2.8}{2.25}{south east}{north west}{(-1.1,-5.4)}{4.4}};
      }},
    %% TRD
    pics/alicetrd/.style = {
      code = {
        \pic at (0,0) {alicefull=TRD};
        \pic at (-1.7,-.05) {%
          alicezoom={TRD}{3.3}{2.8}{south east}{north west}{(-1.05,-5.4)}{4.4}};
      }},
    %% TOF
    pics/alicetof/.style = {
      code = {
        \pic at (0,0) {alicefull=TOF};
        \pic at (-1.8,-.05) {%
          alicezoom={TOF}{3.6}{2.9}{south east}{north west}{(-1,-5.4)}{4.4}};
      }},
    %% HMPID
    pics/alicehmpid/.style = {
      code = {
        \pic at (0,0) {alicefull=HMPID};
        \pic at (-2.5,.4) {%
          alicezoom={HMPID}{1.5}{1.8}{north east}{south west}{(2.2,-6)}{3.2}};
      }},    
    %% EMCAL
    pics/aliceemcal/.style = {
      code = {
        \pic at (0,0) {alicefull=EMCAL};
        \pic at (-1.6,1.4) {%
          alicezoom={EMCAL}{3.5}{1.2}{north east}{south west}{(3,-7.5)}{7}};
      }},
    %% PHOS
    pics/alicephos/.style = {
      code = {
        \pic at (0,0) {alicefull=PHOS};
        \pic at (-2,-1.5) {%
          alicezoom={PHOS}{2}{1}{north east}{south west}{(3,-4.6)}{6}};
      }},
    %% FMD
    pics/alicefmd/.style = {
      code = {
        \pic at (0,0) {alicefull=FMD};
        \pic at (-1.9,.1) {%
          alicezoom={FMD}{2}{1.1}{north east}{south west}{(3.2,-5.8)}{7}};
      }},
    %% FMD
    pics/alicepmd/.style = {
      code = {
        \pic at (0,0) {alicefull=PMD};
        \pic at (-2.8,.1) {%
          alicezoom={PMD}{1.8}{1.1}{north east}{south west}{(3.2,-5.8)}{7}};
      }},
    %% V0
    pics/alicev0/.style = {
      code = {
        \pic at (0,0) {alicefull=V0};
        \pic at (-2,.1) {%
          alicezoom={V0}{1.6}{1.1}{north east}{south west}{(3.3,-5.9)}{7}};
      }},
    %% T0
    pics/alicet0/.style = {
      code = {
        \pic at (0,0) {alicefull=T0};
        \pic at (-2.7,.2) {%
          alicezoom={T0a}{.2}{.25}{south east}{north west}{(-1,-5.4)}{1.5}};
        \pic at (-1.5,-.07) {%
          alicezoom={T0c}{.2}{.25}{north east}{south west}{(3.2,-5.4)}{1.5}};
      }},
    %% ZDC
    pics/alicezdc/.style = {
      code = {
        \pic at (0,0) {alicefull=ZDC};
        \pic at (4.3,-1.1) {%
          alicezoom={ZDCc}{.75}{.4}{south east}{north west}{(-1.8,-4.9)}{3}};
        \pic at (-4.2,.5) {%
          alicezoom={ZDCa}{.75}{.4}{north east}{south west}{(1.8,-6.5)}{3}};
      }},
    %% MUON
    pics/alicemuon/.style = {
      code = {
        \pic at (0,0) {alicefull=MUON};
        \pic at (1.5,-.5) {%
          alicezoom={MUON}{4.3}{3.8}{south east}{north west}{(-4.1,-5)}{4.7}};
      }},
    %% ACORDE
    pics/aliceacorde/.style = {
      code = {
        \pic at (0,0) {alicefull=ACORDE};
        \pic at (-1.2,2.85) {%
          alicezoom={ACORDE}{4}{1.2}{north east}{south west}{(2.7,-8)}{7}};
      }},
    ]
    \pic at (0,0) {alicefull=ALICE};
    \foreach \d [count=\o] in \@lice@dets{%
      \pgfmathtruncatemacro\oo{\o+1}%
      \uncover<\oo>{\pic at (0,0) {alice\d};}}
    %% Grid - for debugging
    \@lice@det@grid
  \end{tikzpicture}
}
%% Local Variables:
%% TeX-master: "alice-detectors"
%% End:
