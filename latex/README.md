LaTeX templates for notes, posters, and presentations
=====================================================

The source is in the file `alice_template.dtx` which should be
processed by the *docstrip* utility.  The driver file
`alice_template.int` does that automatically.

After the *docstrip* utility has processed `alice_template.dtx`, you
will end up with a bunch of files 

Note class and supporting packages
----------------------------------

- `alicenote.cls` - Note class 
- `alicemisc.sty` - supporting package 

Beamer presentation and poster packages
---------------------------------------

Note that these use the package `alicemisc.sty` 

- `beamerthemealice.sty` - the master theme 
- `beamercolorthemealice.sty` - the color scheme
- `beamerinnerthemealice.sty` - inner theming (e.g., lists, blocks,
  etc.) 
- `beamerouterthemealice.sty` - outer theming (e.g., geometry,
  margins, etc.) 
  
Other material
--------------

- `tikzlibraryalicelogo.code.tex` - Tikz library to render the ALICE
  logo using vector graphics. 
- `tikzlibrarycernlogo.code.tex` - Tikz library to render the CERN
  logo using vector graphics. 
  
Examples
--------

Example code using the class and packages can be found in the
sub-directory `examples`. 

- `acknowledgements.tex` - Example acknowledgments source 
- `authorlist.tex` - Example author list source 
- `note_example_foot.tex` - Example of a note with a few authors and
  affiliations in the footnotes. 
- `note_example_line.tex` - Example of a note with a few authors and
  affiliations below the authors. 
- `note_example.tex` - Example official note 
- `poster_example.tex` - Example of a poster 
- `presentation_content.tex` - Example content of a presentation
- `presentation_example_dark.tex` - Example of a presentation with
  dark background 
- `presentation_example_light.tex`  - Example of a presentation with
  white background 
- `testAspect.tex` - Test aspect ration switch 

In the sub-directory `detectors` is a package and supporting graphics
for making slides that show the location of (a sub-set of) all ALICE
detectors.  If you want to use this package, please note that you need
to copy the directory `figs` to your presentation too. 

Christian Holm Christensen
Copenhagen, 2017


