Examples
========

Example code using the class and packages/

- `acknowledgements.tex` - Example acknowledgments source 
- `authorlist.tex` - Example author list source 
- `note_example_foot.tex` - Example of a note with a few authors and
  affiliations in the footnotes. 
- `note_example_line.tex` - Example of a note with a few authors and
  affiliations below the authors. 
- `note_example.tex` - Example official note 
- `poster_example.tex` - Example of a poster 
- `presentation_content.tex` - Example content of a presentation
- `presentation_example_dark.tex` - Example of a presentation with
  dark background 
- `presentation_example_light.tex`  - Example of a presentation with
  white background 
- `testAspect.tex` - Test aspect ration switch 
- `cernlogo.tex` - produces a EPS/PDF of the CERN logo
